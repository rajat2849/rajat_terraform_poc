provider "aws" {
  region     = "${var.region}"
  profile = "poc"
 
} 

terraform {
  backend "s3" {
    bucket = "pristine-poc-tf.state-file"
    key = "pristine_poc/jenkins/jenkins_pristine-poc.tfstate"
    region = "us-east-1"    
  }
  
}