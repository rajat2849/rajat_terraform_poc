#!/bin/bash
sudo yum update -y
# sudo yum install docker -y
# sudo usermod -a -G docker ec2-user
# sudo chmod 777 /var/run/docker.sock
# sudo systemctl start docker
# sudo chmod 777 /var/run/docker.sock
# sudo docker run -p 8080:8080 --name=jenkins_poc -d jenkins/jenkins
# sudo docker exec jenkins_poc cat /var/jenkins_home/secrets/initialAdminPassword
sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key    
sudo yum upgrade -y
sudo amazon-linux-extras install java-openjdk11 -y
sudo yum install jenkins -y
sudo systemctl enable jenkins
sudo systemctl start jenkins
