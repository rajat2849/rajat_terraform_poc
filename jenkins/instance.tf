#create EC2 intance
resource "aws_instance" "tf-instance" {
  ami                    = "${var.image}"
  instance_type          = "${var.instance_type}"
  key_name               = aws_key_pair.tf-key.key_name          #key-pair
  vpc_security_group_ids = ["${aws_security_group.tf-instance.id}"] #security group
    tags = merge(
     var.additional_tags
  )  

  user_data = file("${path.module}/script.sh") #pre applied commond run just after craete intance
}
output "ec2_global_ips" {
  value = ["${aws_instance.tf-instance.*.public_ip}"]
}