resource "aws_s3_bucket" "terraform-file" {
    bucket =  "${var.s3_bucket_name}"
    lifecycle {
      prevent_destroy = false 
    }    
}
resource "aws_s3_bucket_versioning" "versioning_config" {
  bucket = aws_s3_bucket.terraform-file.id
  versioning_configuration {
    status = "Enabled"
  }
}