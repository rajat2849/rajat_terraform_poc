variable "ports" {
    type = list(number)
}

variable "image" {
    type = string  
}

variable "instance_type" {
    type = string
}



variable "region" {
    type = string 
}

variable "s3_bucket_name" {
    type = string
}

variable "additional_tags" {
    type = map(string) 
    default = {
    "Terraform"     = "true"
    "Project"       = "Pristine"
    "Organization"  = "Kellton"
    "Name"          = "pristine-Jenkins-poc-by-Rahul Mishra"
    "Environment"   = "POC"
    "Approved_By"   = "Suraj Kumar"
    "Expiration_by" = "Jan/2023"
  }
}