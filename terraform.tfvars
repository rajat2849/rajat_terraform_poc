# .env
region = "us-east-1"
availabilityzone     = ["us-east-1a" , "us-east-1b", "us-east-1c"]
name_prefix   = "pristine_poc_"

#MySql Database Variable
db_name    = "POCdb"
dbuser     = "admin"
dbpassword = "password"
access_ip  = "0.0.0.0/0"
db_instance_class    = "db.t2.micro"
db_engine_version    = "8.0"
db_identifier        = "pristine-poc-db"
db_port = "3306"

#Web Server Variable (EC2)
instance_type = "t2.micro"
vpc_cidr      = "10.0.0.0/16"
image_id      = "ami-0dd837260b75b223f"
ami_bastion   = "ami-0fe472d8a85bc7b0e"

#Redis Server Variable
cluster_id           = "pristine-poc-redis"
engine               = "redis"
node_type            = "cache.t2.micro"
# parameter_group_name = "default.redis6.0"
engine_version       = "6.0"

#cloudfront variable 
# domain_name       = "www.example.com"
# validation_method = "DNS"
api_origin_id     = "api-alb"

#IAM variable
name = "pristine_poc_policy"

#S3 variable 
s3_origin_id = "pristine_poc_origin"

#Cloud Watch variable
alarm_name                = "pristine_poc_cpu_utilization"
comparison_operator       = "GreaterThanOrEqualToThreshold"
evaluation_periods        = "2"
metric_name               = "CPUUtilization"
namespace                 = "AWS/EC2"
period                    = "120" #seconds
statistic                 = "Average"
threshold                 = "80"
alarm_description         = "This metric monitors ec2 cpu utilization"






