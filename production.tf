provider "aws" {
  region     = var.region
  profile = "poc"
}
resource "random_id" "random_id_prefix" {
  byte_length = 2
}
terraform {
  backend "s3" {
    bucket = "pristine-poc-tf.state-file"
    key = "pristine_poc/pristine-poc.tfstate"
    region = "us-east-1"    
  }
  
}
module "Networking" {
  source               = "./modules/Networking"
  vpc_cidr             = var.vpc_cidr
  access_ip            = var.access_ip
  public_sn_count      = 2
  private_sn_count     = 2
  db_subnet_group      = true
  availabilityzone     = var.availabilityzone
  azs                  = 2
  additional_tags      = var.additional_tags
  role                 = module.IAM.pristine_poc_role
  vpn_id               = module.VPN.vpn_id
  name_prefix          = var.name_prefix
}

module "S3" {
  source              = "./modules/S3"
  additional_tags     = var.additional_tags
  name_prefix         = var.name_prefix
  
  
}

module "Database" {
  source               = "./modules/Database"
  db_storage           = 8
  db_engine_version    = var.db_engine_version
  db_name              = var.db_name
  db_instance_class    = var.db_instance_class
  dbuser               = var.dbuser
  dbpassword           = var.dbpassword
  db_identifier        = var.db_identifier
  db_port              = var.db_port
  skip_db_snapshot     = true
  rds_sg               = module.Networking.rds_sg
  db_subnet_group_name = module.Networking.db_subnet_group_name[0]
  # availabilityzone     = module.Networking.availabilityzone
  additional_tags      = var.additional_tags
  awssnstopic          = module.SNS.awssnstopic 
  name_prefix          = var.name_prefix

}

module "compute" {
  source                   = "./modules/compute"
  Web_Server_sg            = module.Networking.Web_Server_sg
  bastion_sg               = module.Networking.bastion_sg
  public_subnets           = module.Networking.public_subnets
  private_subnets          = module.Networking.private_subnets
  bastion_instance_count   = 1
  instance_type            = var.instance_type
  lb_tg                    = module.loadbalancing.lb_tg
  lb_tg_name               = module.loadbalancing.lb_tg_name
  image_id                 = var.image_id
  name_prefix              = var.name_prefix
  additional_tags          = var.additional_tags
  awssnstopic              = module.SNS.awssnstopic
  ami_bastion              = var.ami_bastion
  # availabilityzone         = module.Networking.availabilityzone
}

module "loadbalancing" {
  source                             = "./modules/loadbalancing"
  lb_sg                              = module.Networking.lb_sg
  public_subnets                     = module.Networking.public_subnets
  tg_port                            = 80
  tg_protocol                        = "HTTP"
  vpc_id                             = module.Networking.vpc_id
  pristine_poc_autoscaling_group_id  = module.compute.pristine_poc_autoscaling_group_id
  listener_port                      = 80
  listener_protocol                  = "HTTP"
  azs                                = 2
  additional_tags                    = var.additional_tags
  name_prefix                        = var.name_prefix

}

module "redis" {
  source                             = "./modules/redis"
  cluster_id                         = var.cluster_id
  engine                             = var.engine
  node_type                          = var.node_type
  # parameter_group_name               = var.parameter_group_name
  engine_version                     = var.engine_version
  redis_sg                           = module.Networking.redis_sg
  elasticache_subnet_group           = module.Networking.elasticache_subnet_group[0]
  additional_tags                    = var.additional_tags
  awssnstopic                        = module.SNS.awssnstopic
  name_prefix                        = var.name_prefix

}

module "cloudfront" {
  source                           = "./modules/cloudfront"
  additional_tags                  = var.additional_tags
  # domain_name                     = var.domain_name
  # validation_method               = var.validation_method
  s3                              = module.S3.s3
  s3_origin_id                    = var.s3_origin_id
  alb_dns                         = module.loadbalancing.alb_dns
  api_origin_id                   = var.api_origin_id
  waf_acl                         = module.WAF.waf_acl
  name_prefix                     = var.name_prefix

}

module "IAM" {
  source                         = "./modules/IAM"
  additional_tags                = var.additional_tags
  name                           = var.name
  name_prefix                    = var.name_prefix

}

module "cloudtrail" {
  source                        = "./modules/cloudtrail"
  s3_cloudtrail                 = module.S3.s3_cloudtrail
  additional_tags               = var.additional_tags
  name_prefix                   = var.name_prefix 
}

module "aws_config" {
  source                        = "./modules/aws_config"
  s3_aws_config                 = module.S3.s3_aws_config
  aws_config_role               = module.S3.aws_config_role
  additional_tags               = var.additional_tags
  name_prefix                   = var.name_prefix

}
module "SSM" {
  source                         = "./modules/SSM"
  additional_tags                = var.additional_tags
  name_prefix                   = var.name_prefix

}
module "SNS" {
  source                        = "./modules/SNS"
  additional_tags               = var.additional_tags
  name_prefix                   = var.name_prefix

}
module "cloudwatch" {
  source                              = "./modules/cloudwatch"
  sns_arn                             = module.SNS.awssnstopic
  pristine_poc_lb                     = module.loadbalancing.pristine_poc_lb
  pristine_poc_tg                     = module.loadbalancing.pristine_poc_tg
  additional_tags                     = var.additional_tags
  alarm_name                          = var.alarm_name
  comparison_operator                 = var.comparison_operator
  evaluation_periods                  = var.evaluation_periods
  metric_name                         = var.metric_name
  namespace                           = var.namespace
  period                              = var.period
  statistic                           = var.statistic
  threshold                           = var.threshold
  alarm_description                   = var.alarm_description
  pristine_poc_autoscaling_group_name = module.compute.pristine_poc_autoscaling_group_name
  name_prefix                         = var.name_prefix


}
module "VPN" {
  source                             = "./modules/VPN"
  pristine_poc_tgw                   = module.Networking.pristine_poc_tgw
  # aws_acm_certificate               = module.cloudfront.aws_acm_certificate
  additional_tags                    = var.additional_tags
  name_prefix                        = var.name_prefix

}

module "WAF" {
  source                             = "./modules/WAF"  
  additional_tags                    = var.additional_tags
  pristine_poc_lb                    =  module.loadbalancing.pristine_poc_lb_arn
  name_prefix                        = var.name_prefix
  
}
# module "CodeDeploy" {
#   source = "./modules/CodeDeploy"
  
# }