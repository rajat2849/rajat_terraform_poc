## AWS S3 bucket
resource "aws_s3_bucket" "s3_aws_config" {

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Config"
    }
  )   
}
resource "aws_s3_bucket_acl" "acl_config" {
  bucket = aws_s3_bucket.s3_aws_config.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "versioning_config" {
  bucket = aws_s3_bucket.s3_aws_config.id
  versioning_configuration {
    status = "Enabled"
  }
}

## IAM role
resource "aws_iam_role" "aws_config_role"{
  name = "config-capoc"
  
  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
            "Service": "config.amazonaws.com"
        },
       "Effect": "Allow",
       "Sid": ""
       }
    ]
}
POLICY
}

## Attaching polices toabove role
resource "aws_iam_role_policy_attachment" "my-config" {
  role = aws_iam_role.aws_config_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWS_ConfigRole"
}
# Adding policy for delivery channel
resource "aws_iam_role_policy" "p"{
  name = "awsconfig-capocs"
  role = aws_iam_role.aws_config_role.name
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
      "s3:*"
     ],
     "Effect": "Allow",
     "Resource": [
      "${aws_s3_bucket.s3_aws_config.arn}",
      "${aws_s3_bucket.s3_aws_config.arn}/*"
     ]
   }
  ] 
} 
POLICY
}