
resource "aws_s3_bucket" "s3" {
  force_destroy =  true
  
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Cloudfront"
    }
  )  
}

resource "aws_s3_bucket_public_access_block" "s3_private_access" {
  bucket = "${aws_s3_bucket.s3.id}"
  block_public_acls = true
  block_public_policy = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_ownership_controls" "s3_ownership" {
  bucket = aws_s3_bucket.s3.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}


resource "aws_s3_bucket_acl" "acl" {
  bucket = aws_s3_bucket.s3.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.s3.id
  versioning_configuration {
    status = "Enabled"
  }
}

