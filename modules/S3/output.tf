output "s3" {
    value = aws_s3_bucket.s3.bucket_regional_domain_name 
}

output "s3_cloudtrail" {
  value = aws_s3_bucket.s3_cloudtrail.id
}

output "s3_aws_config" {
    value = aws_s3_bucket.s3_aws_config.id
}
output "aws_config_role" {
    value = aws_iam_role.aws_config_role.arn
}