data "aws_caller_identity" "current" {}
resource "aws_s3_bucket" "s3_cloudtrail"{
   force_destroy = true

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_CloudTrail"
    }
  )
}

resource "aws_s3_bucket_policy" "bucket_policy" {
    bucket =  aws_s3_bucket.s3_cloudtrail.id

    policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "${aws_s3_bucket.s3_cloudtrail.arn}"
        },
        {
            "Sid": "AWSCloudTrailWrite",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "${aws_s3_bucket.s3_cloudtrail.arn}/cloudtrailkey/AWSLogs/${data.aws_caller_identity.current.account_id}*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
POLICY
}