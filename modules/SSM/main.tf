## create SSM document
resource "aws_ssm_document" "cqpocsssmdocument"{
  name = "pristine_poc_document"
  document_type = "Command"

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_SSM_Documnet"
    }
  )
  content = <<DOC
  {
    "schemaVersion": "1.2",
    "description": "Check ip configuration of a Linux instance.",
    "parameters": {

    },
    "runtimeConfig": {
      "aws:runShellScript": {
        "properties": [
          {
            "id": "0.aws:runShellScript",
            "runCommand": ["ifconfig"]
          }
        ]
      }
    }
  }
DOC
}

## Create SSM Association
resource "aws_ssm_association" "cqpocsssmassociation"{
    name = aws_ssm_document.cqpocsssmdocument.name
    schedule_expression = "cron(0 0 0 ? * *)"
    targets {
        key = "tag:run_ssm_document"
        values = ["yes"]
    }
  #   targets {
  #   key    = "InstanceIds"
  #   values = [aws_instance.example.id]
  # }
    
}
