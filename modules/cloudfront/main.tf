
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
    comment = "origin acces identity"   
}

resource "aws_cloudfront_distribution" "pristine_poc_cloudfront" {
    origin {
      domain_name = var.s3
      origin_id  = var.s3_origin_id
      s3_origin_config {
        origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
      }        
    }

   origin {
      domain_name = var.alb_dns
      origin_id   = var.api_origin_id
      custom_origin_config {
        http_port              = 80
        https_port             = 443
        origin_protocol_policy = "http-only"
        origin_ssl_protocols   = ["TLSv1.2"]
      }
   }

  enabled             = true
  default_root_object = "index.html"
  custom_error_response {
    error_code         = 403
    response_code      = 200
    response_page_path = "/index.html"
  }

  default_cache_behavior {
    viewer_protocol_policy = "redirect-to-https"
    compress               = true
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    // This needs to match the `origin_id` above.
    target_origin_id = var.s3_origin_id
    min_ttl          = 0
    default_ttl      = 86400
    max_ttl          = 31536000

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  ordered_cache_behavior {
    path_pattern     = "/bulkops/*"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = var.s3_origin_id

    default_ttl = 0
    min_ttl     = 0
    max_ttl     = 0

    forwarded_values {
      query_string = true
      cookies {
        forward = "all"
      }
    }
    viewer_protocol_policy = "redirect-to-https"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  } 

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  web_acl_id = var.waf_acl

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_CloudFront"
    }
  )  
}