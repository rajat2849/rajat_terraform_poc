resource "aws_acm_certificate" "aws_acm_certificate" {
  private_key = file("./pristine_ssl_2023/pristineconnect.key")
  certificate_body =file("./pristine_ssl_2023/2b0c092a73c41c9f.crt")
  certificate_chain = file("./pristine_ssl_2023/gd_bundle-g2-g1.crt")
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_ACM_Certificate"
    }
  ) 
}
