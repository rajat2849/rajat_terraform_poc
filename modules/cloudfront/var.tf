variable "additional_tags" {}
# variable "domain_name" {}
# variable "validation_method" {}
variable "s3" {}
variable "s3_origin_id" {}
variable "alb_dns" {}
variable "api_origin_id" {}
variable "waf_acl" {}
variable "name_prefix" {}
