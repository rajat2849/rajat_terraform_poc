variable "redis_sg" {}
variable "elasticache_subnet_group" {
    type = string
}
variable "cluster_id" {}
variable "engine" {}
variable "engine_version" {}
# variable "parameter_group_name" {}
variable "node_type" {}
variable "additional_tags" {}
variable "awssnstopic" {}
variable "name_prefix" {
  
}
