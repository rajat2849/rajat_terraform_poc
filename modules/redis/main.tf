resource "aws_elasticache_cluster" "pristine_poc_redis" {
  cluster_id           = var.cluster_id
  engine               = var.engine
  node_type            = var.node_type
  num_cache_nodes      = 1
  # parameter_group_name = var.parameter_group_name
  engine_version       = var.engine_version
  port                 = 6379
  security_group_ids = [var.redis_sg]
  subnet_group_name = var.elasticache_subnet_group
  notification_topic_arn = var.awssnstopic

  

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Redis"
    }
  )  
}