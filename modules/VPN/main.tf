resource "aws_customer_gateway" "customer_gateway" {
  bgp_asn    = 64514
  ip_address = "1.6.151.77"
  type       = "ipsec.1"
  # certificate_arn = var.aws_acm_certificate

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Customer_Gateway"
    }
  )
}

resource "aws_vpn_connection" "vpn" {
    customer_gateway_id = aws_customer_gateway.customer_gateway.id
    outside_ip_address_type = "PublicIpv4"
    transit_gateway_id = var.pristine_poc_tgw
    type = "ipsec.1"
    
    static_routes_only = false #Static routes must be used for devices that don't support BGP.
    # local_ipv4_network_cidr = "1.6.151.77" #The IPv4 CIDR on the customer gateway (on-premises) side of the VPN connection.

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_VPN"
    }
  )

}