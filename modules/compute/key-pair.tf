#key_pair
resource "tls_private_key" "pk" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "tf-key" {
  key_name   = "pristin_key"       # Create a "tfKey" to AWS!!
  public_key = tls_private_key.pk.public_key_openssh

  provisioner "local-exec" { # Create a "tfKey.pem" to your computer!!
    command = "echo '${tls_private_key.pk.private_key_pem}' > ./pristin_Key.pem"
  }
}