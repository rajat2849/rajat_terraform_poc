# --- compute/variables.tf ---

variable "bastion_sg" {}
variable "Web_Server_sg" {
  type = string 
}
variable "private_subnets" {}
variable "public_subnets" {}
# variable "key_name" {}
variable "lb_tg_name" {}
variable "lb_tg" {}
variable "bastion_instance_count" {
  type = number
}
variable "instance_type" {
  type = string
}
variable "image_id" {}
variable "name_prefix" {
  type = string 
}
variable "additional_tags" {}
variable "awssnstopic" {}
variable "ami_bastion" {}
# variable "availabilityzone" {}
