# data "aws_ssm_parameter" "pristine_poc-ami" {
#   name = var.image_id
# }


# LAUNCH BASTION

resource "aws_instance" "pristine_poc_bastion" {
  instance_type               = var.instance_type
  ami                         = var.ami_bastion
  vpc_security_group_ids      = ["${var.bastion_sg}"]
  key_name                    = aws_key_pair.tf-key.key_name
  subnet_id                   = "${var.public_subnets[0]}"
  associate_public_ip_address = true
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}Bastion_Server"
    }
  )
}

# LAUNCH TEMPLATES AND AUTOSCALING GROUPS FOR WEB APP TIER

resource "aws_launch_template" "web_server" {
  name_prefix            = "${var.name_prefix}Web_Server"
  instance_type          = var.instance_type
  image_id               = var.image_id
  # vpc_security_group_ids = [var.Web_Server_sg]
#   user_data              = filebase64("install_apache.sh")
  key_name               = aws_key_pair.tf-key.key_name

  network_interfaces {
    subnet_id = "${var.private_subnets[0]}"
    security_groups = [var.Web_Server_sg]
  }
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Web_Server"
    }
  )
}

