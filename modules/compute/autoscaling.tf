resource "aws_autoscaling_group" "pristine_poc_autoscaling_group" {
  name                = "Pristine_POC_AutoSacling_Group"
  vpc_zone_identifier = var.private_subnets
  min_size            = 1
  max_size            = 3
  desired_capacity    = 2
  # availability_zones  = var.availabilityzone
  target_group_arns   = [data.aws_lb_target_group.pristine_poc_tg.arn]

  launch_template {
    id      = aws_launch_template.web_server.id
    version = "$Latest"
  }
  
}

resource "aws_autoscaling_notification" "pristin_poc_autosacalinggroup_notification" {
    group_names = [
        aws_autoscaling_group.pristine_poc_autoscaling_group.name
    ]

  notifications = [
    "autoscaling:EC2_INSTANCE_LAUNCH",
    "autoscaling:EC2_INSTANCE_TERMINATE",
    "autoscaling:EC2_INSTANCE_LAUNCH_ERROR",
    "autoscaling:EC2_INSTANCE_TERMINATE_ERROR",
  ]

  topic_arn = var.awssnstopic
}

# AUTOSCALING ATTACHMENT FOR APP TIER TO LOADBALANCER

data "aws_lb_target_group" "pristine_poc_tg" {
  name = var.lb_tg_name
}

resource "aws_autoscaling_attachment" "asg_attach" {
  autoscaling_group_name = aws_autoscaling_group.pristine_poc_autoscaling_group.id
  lb_target_group_arn    = var.lb_tg
}