# MySql DB Instance
resource "aws_db_instance" "pristine_poc_db" {
  allocated_storage      = var.db_storage
  engine                 = "mysql"
  engine_version         = var.db_engine_version
  instance_class         = var.db_instance_class
  db_name                = var.db_name
  username               = var.dbuser
  password               = var.dbpassword
  db_subnet_group_name   = var.db_subnet_group_name
  identifier             = var.db_identifier
  skip_final_snapshot    = var.skip_db_snapshot
  vpc_security_group_ids = [var.rds_sg]
  port                   = var.db_port
  # availability_zone      = var.availabilityzone
  

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_RDS_Mysql"
    }
  )
}

#Amazon Simple Notification Service for MySql DB Instance
resource "aws_db_event_subscription" "pristine_poc_db_sns" {
  name = "Pristine-Poc-Db-SNS"
  sns_topic = var.awssnstopic

  source_type = "db-instance"
  source_ids = [aws_db_instance.pristine_poc_db.id]

  event_categories = [
    "availability",
    "deletion",
    "failover",
    "failure",
    "low storage",
    "maintenance",
    "notification",
    "read replica",
    "recovery",
    "restoration",
  ] 
}