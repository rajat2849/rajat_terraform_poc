# --- loadbalancing/main.tf ---


# INTERNET FACING LOAD BALANCER

resource "aws_lb" "pristine_poc_lb" {
  security_groups = [var.lb_sg]
  subnets         = var.public_subnets
  idle_timeout    = 400
  

  depends_on = [
    var.pristine_poc_autoscaling_group_id
  ]
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_LoadBalancer"
    }
  )
}

resource "aws_lb_target_group" "pristine_poc_tg" {
  port     = var.tg_port
  protocol = var.tg_protocol
  vpc_id   = var.vpc_id

  lifecycle {
    ignore_changes        = [name]
    create_before_destroy = true
  }
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Traget_Group-${substr(uuid(), 0, 3)}"
    }
  )
}

resource "aws_lb_listener" "pristine_poc_lb_listener" {
  load_balancer_arn = aws_lb.pristine_poc_lb.arn
  port              = var.listener_port
  protocol          = var.listener_protocol
  
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.pristine_poc_tg.arn
  }
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_LB_Listener"
    }
  )
}