# --- loadbalancing/outputs.tf --- 

output "alb_dns" {
  value = aws_lb.pristine_poc_lb.dns_name
}

output "pristine_poc_lb_arn" {
  value = aws_lb.pristine_poc_lb.arn
}

output "lb_tg_name" {
  value = aws_lb_target_group.pristine_poc_tg.name
}

output "lb_tg" {
  value = aws_lb_target_group.pristine_poc_tg.arn
}

output "pristine_poc_tg" {
  value = aws_lb_target_group.pristine_poc_tg.arn_suffix
}

output "pristine_poc_lb" {
  value = aws_lb.pristine_poc_lb.arn_suffix
}