# --- loadbalancing/variables.tf ---

variable "lb_sg" {}
variable "public_subnets" {}
variable "pristine_poc_autoscaling_group_id" {}
variable "tg_port" {}
variable "tg_protocol" {}
variable "vpc_id" {}
variable "listener_port" {}
variable "listener_protocol" {}
variable "azs" {}
variable "additional_tags" {}
variable "name_prefix" {}
