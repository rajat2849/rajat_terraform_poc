# --- networking/variables.tf ---

variable "vpc_cidr" {
  type = string
}

variable "public_sn_count" {
    type = number
 
}

variable "private_sn_count" {
    type = number

}

variable "access_ip" {
  type = string
}

variable "db_subnet_group" {
  type = bool
}

variable "availabilityzone" {
}
variable "azs" {}
variable "additional_tags" {}
variable "role" {
}
variable "vpn_id" {}
variable "name_prefix" {}