resource "aws_flow_log" "pristine_poc_" {
  iam_role_arn = var.role
  log_destination = aws_cloudwatch_log_group.pristine_poc_log_group.arn
  traffic_type = "ALL"
  # vpc_id = aws_vpc.pristine_poc_vpc.id
  transit_gateway_id = aws_ec2_transit_gateway.pristine_poc_tgw.id
  max_aggregation_interval = "60"
  
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_FlowLog"
    }
  )  
}

resource "aws_cloudwatch_log_group" "pristine_poc_log_group" {
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_FlowLog_Log_Group"
    }
  )
}