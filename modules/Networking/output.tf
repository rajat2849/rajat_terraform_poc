# --- networking/outputs.tf ---

output "vpc_id" {
  value = aws_vpc.pristine_poc_vpc.id
}

output "db_subnet_group_name" {
  value = aws_db_subnet_group.pristine_poc_rds_subnetgroup.*.name
}

output "rds_db_subnet_group" {
  value = aws_db_subnet_group.pristine_poc_rds_subnetgroup.*.id
}

output "rds_sg" {
  value = aws_security_group.pristine_poc_rds_sg.id
}

output "Web_Server_sg" {
  value = aws_security_group.pristine_poc_Web_Server_sg.id
}



output "bastion_sg" {
  value = aws_security_group.pristine_poc_bastion_sg.id
}

output "lb_sg" {
  value = aws_security_group.pristine_poc_lb_sg.id
}

output "public_subnets" {
  value = aws_subnet.pristine_poc_public_subnets.*.id
}

output "private_subnets" {
  value = aws_subnet.pristine_poc_private_subnets.*.id
}

output "private_subnets_db" {
  value = aws_subnet.pristine_poc_private_subnets_db.*.id
}
# output "availabilityzone" {
#   value = aws_subnet.pristine_poc_private_subnets_db.*.availability_zone
# }

output "redis_sg" {
  value = aws_security_group.pristine_poc_redis_sg.id
}

output "elasticache_subnet_group" {
  value = aws_elasticache_subnet_group.pristine_poc_elasticache_subnet_group.*.id
  
}

output "pristine_poc_tgw" {
  value = aws_ec2_transit_gateway.pristine_poc_tgw.id
  
}