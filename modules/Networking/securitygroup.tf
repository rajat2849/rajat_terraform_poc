resource "aws_security_group" "pristine_poc_rds_sg" {
  description = "Allow MySQL Port Inbound Traffic from Backend App Security Group"
  vpc_id      = aws_vpc.pristine_poc_vpc.id

  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.pristine_poc_Web_Server_sg.id]
  }
  ingress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    security_groups = [aws_security_group.pristine_poc_bastion_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_RDS_SG"
    }
  )
}

resource "aws_security_group" "pristine_poc_bastion_sg" {
  description = "Allow SSH Inbound Traffic From Set IP"
  vpc_id      = aws_vpc.pristine_poc_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.access_ip]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Bastion_SG"
    }
  )
}

resource "aws_security_group" "pristine_poc_lb_sg" {
  description = "Allow Inbound HTTP Traffic"
  vpc_id      = aws_vpc.pristine_poc_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_LB_SG"
    }
  )
}

resource "aws_security_group" "pristine_poc_Web_Server_sg" {
  description = "Allow SSH inbound traffic from Bastion, and HTTP inbound traffic from loadbalancer"
  vpc_id      = aws_vpc.pristine_poc_vpc.id

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.pristine_poc_bastion_sg.id]
  }

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.pristine_poc_lb_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Web_Server_SG"
    }
  )
}



resource "aws_security_group" "pristine_poc_redis_sg" {
  description = "Allow Redis Port Inbound Traffic from Backend App Security Group"
  vpc_id      = aws_vpc.pristine_poc_vpc.id

  ingress {
    from_port       = 6379
    to_port         = 6379
    protocol        = "tcp"
  
}
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Radis_SG"
    }
  )
  }