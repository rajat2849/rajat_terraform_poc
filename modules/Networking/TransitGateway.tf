#Create TGW
resource "aws_ec2_transit_gateway" "pristine_poc_tgw" {
  
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Transit_Gateway"
    }
  )
}

resource "aws_ec2_transit_gateway_route_table" "association_default_route_table" {
  transit_gateway_id = aws_ec2_transit_gateway.pristine_poc_tgw.id
    tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_TG_RT"
    }
  )
}


# TGW Route Table
resource "aws_ec2_transit_gateway_route" "tgw_default_route" {
  destination_cidr_block         = "0.0.0.0/0"
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.pristine_poc_tgw_attach_edge.id
  transit_gateway_route_table_id = aws_ec2_transit_gateway.pristine_poc_tgw.association_default_route_table_id

  
}

data "aws_ec2_transit_gateway_vpn_attachment" "tgw_vpn" {
  transit_gateway_id = aws_ec2_transit_gateway.pristine_poc_tgw.id
  vpn_connection_id = var.vpn_id
  
}