### CUSTOM VPC CONFIGURATION

resource "random_integer" "random" {
  min = 1
  max = 100
}

resource "aws_vpc" "pristine_poc_vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_VPC"
    }
  )
  lifecycle {
    create_before_destroy = true
  }
}

data "aws_availability_zones" "available" {
}

### INTERNET GATEWAY

resource "aws_internet_gateway" "pristine_poc_internet_gateway" {
  vpc_id = aws_vpc.pristine_poc_vpc.id

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Internet_Gateway"
    }
  )
  lifecycle {
    create_before_destroy = true
  }
}


### PUBLIC SUBNETS (WEB TIER) AND ASSOCIATED ROUTE TABLES

resource "aws_subnet" "pristine_poc_public_subnets" {
  count                   = var.public_sn_count
  vpc_id                  = aws_vpc.pristine_poc_vpc.id
  cidr_block              = "10.0.${10 + count.index}.0/24"
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[count.index]

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Public_Subnet"
    }
  )
  
}

resource "aws_route_table" "pristine_poc_public_rt" {
  vpc_id = aws_vpc.pristine_poc_vpc.id

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Route_Table"
    }
  )
}

resource "aws_route" "default_public_route" {
  route_table_id         = aws_route_table.pristine_poc_public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.pristine_poc_internet_gateway.id
}

resource "aws_route_table_association" "pristine_poc_public_assoc" {
  count          = var.public_sn_count
  subnet_id      = aws_subnet.pristine_poc_public_subnets.*.id[count.index]
  route_table_id = aws_route_table.pristine_poc_public_rt.id

}


### EIP AND NAT GATEWAY

resource "aws_eip" "pristine_poc_nat_eip" {
  vpc = true
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_EIP"
    }
  )
}

resource "aws_nat_gateway" "pristine_poc_ngw" {
  allocation_id     = aws_eip.pristine_poc_nat_eip.id
  subnet_id         = aws_subnet.pristine_poc_public_subnets[1].id

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Nat_Gateway"
    }
  )
}


### PRIVATE SUBNETS (APP TIER & DATABASE TIER) AND ASSOCIATED ROUTE TABLES

resource "aws_subnet" "pristine_poc_private_subnets" {
  count                   = var.private_sn_count
  vpc_id                  = aws_vpc.pristine_poc_vpc.id
  cidr_block              = "10.0.${20 + count.index}.0/24"
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[count.index]

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Private_Subnet"
    }
  )
}

resource "aws_route_table" "pristine_poc_private_rt" {
  vpc_id = aws_vpc.pristine_poc_vpc.id
  
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Private_RouteTable"
    }
  )
}

resource "aws_route" "default_private_route" {
  route_table_id         = aws_route_table.pristine_poc_private_rt.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = aws_nat_gateway.pristine_poc_ngw.id
}


resource "aws_route_table_association" "pristine_poc_private_assoc" {
  count          = var.private_sn_count
  route_table_id = aws_route_table.pristine_poc_private_rt.id
  subnet_id      = aws_subnet.pristine_poc_private_subnets.*.id[count.index]
}


resource "aws_subnet" "pristine_poc_private_subnets_db" {
  count                   = var.private_sn_count
  vpc_id                  = aws_vpc.pristine_poc_vpc.id
  cidr_block              = "10.0.${40 + count.index}.0/24"
  map_public_ip_on_launch = false
  availability_zone       = "${data.aws_availability_zones.available.names[count.index]}"

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Private_Subnet_DB"
    }
  )
}
resource "aws_db_subnet_group" "pristine_poc_rds_subnetgroup" {
  count      = var.db_subnet_group == true ? 1 : 0
  subnet_ids = [aws_subnet.pristine_poc_private_subnets_db[0].id, aws_subnet.pristine_poc_private_subnets_db[1].id]

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_RDS_Subnet_Group"
    }
  )
}

resource "aws_elasticache_subnet_group" "pristine_poc_elasticache_subnet_group" {
  name = "pristinepocelasticachesubnetgroup"
  count      = var.db_subnet_group == true ? 1 : 0
  subnet_ids = [aws_subnet.pristine_poc_private_subnets_db[0].id, aws_subnet.pristine_poc_private_subnets_db[1].id]

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Radis_Subnet_Group"
    }
  )
}

# Attach  TGW to  VPC
resource "aws_ec2_transit_gateway_vpc_attachment" "pristine_poc_tgw_attach_edge" {
  subnet_ids         = [aws_subnet.pristine_poc_private_subnets[0].id, aws_subnet.pristine_poc_private_subnets[1].id]
  transit_gateway_id = aws_ec2_transit_gateway.pristine_poc_tgw.id
  vpc_id             = aws_vpc.pristine_poc_vpc.id

  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_TrasitGateway_Attach"
    }
  )
}
