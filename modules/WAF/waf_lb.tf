resource "aws_wafregional_rate_based_rule" "wafrule" {
  name        = "pristine_poc_wafrule"
  metric_name = "pristinepocwafrule"
  rate_key    = "IP"
  rate_limit  = 100000
}

resource "aws_wafregional_web_acl" "wafacl" {
  name        = "pristine_poc_wafrule"
  metric_name = "pristinepocwafrule"

  default_action {
    type = "ALLOW"
  }

  rule {
    action {
      type = "BLOCK"
    }
    priority = 1
    rule_id  = "${aws_wafregional_rate_based_rule.wafrule.id}"
    type     = "RATE_BASED"
  }
  tags = merge(
    var.additional_tags
  )
}



resource "aws_wafregional_web_acl_association" "wafaclassociation" {
  resource_arn = var.pristine_poc_lb
  web_acl_id   = aws_wafregional_web_acl.wafacl.id
}
