resource "aws_waf_ipset" "ipset" {
  name = "pristine_poc_IPSet"

  ip_set_descriptors {
    type  = "IPV4"
    value = "10.0.0.0/24"
  }
  
}

resource "aws_waf_rule" "wafrule" {
  name = "Pristine_POC_WAF_Rule"
  depends_on  = [aws_waf_ipset.ipset]
  metric_name = "pristinepocWAFRule"

  predicates {
    data_id = aws_waf_ipset.ipset.id
    negated = false
    type    = "IPMatch"
  }
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_WAF_Rule"
    }
  )
}

resource "aws_waf_web_acl" "waf_acl" {
  name = "Pristine_POC_Web_ACL"  
  depends_on = [
    aws_waf_ipset.ipset,
    aws_waf_rule.wafrule,
  ]
  metric_name = "pristinepocWebACL"

  default_action {
    type = "ALLOW"
  }

  rules {
    action {
      type = "BLOCK"
    }

    priority = 1
    rule_id  = aws_waf_rule.wafrule.id
    type     = "REGULAR"
  }
  
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_WebACL"
    }
  )

}