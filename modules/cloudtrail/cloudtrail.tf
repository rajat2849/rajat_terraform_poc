resource "aws_cloudtrail" "pristine_poc_autoscaling_group_id" {
    name = "pristine_poc_cloud_trail-1"
    s3_bucket_name = var.s3_cloudtrail
    s3_key_prefix = "cloudtrailkey"
    include_global_service_events = false
    event_selector{
      read_write_type = "All"
      include_management_events = true

    data_resource {
        type = "AWS::S3::Object"
        values = ["arn:aws:s3"]
    }
  }
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_CloudTrail"
    }
  )
}

