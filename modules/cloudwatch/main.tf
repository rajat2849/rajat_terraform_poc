resource "aws_cloudwatch_metric_alarm" "ec2_cpu" {
     alarm_name                = var.alarm_name
     comparison_operator       = var.comparison_operator
     evaluation_periods        = var.evaluation_periods
     metric_name               = var.metric_name
     namespace                 = var.namespace
     period                    = var.period
     statistic                 = var.statistic
     threshold                 = var.threshold
     alarm_description         = var.alarm_description
     alarm_actions             = [var.sns_arn]
     ok_actions                = [var.sns_arn]
     insufficient_data_actions = []
     dimensions = {
       TargetGroup = var.pristine_poc_tg
       LoadBalancer = var.pristine_poc_lb
       autoscaling_group_name = var.pristine_poc_autoscaling_group_name
     }
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_CloudWatch_Alarm"
    }
  )
}

resource "aws_cloudwatch_log_group" "EC2_logGroup" {
  retention_in_days = 30
  
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Log_Group"
    }
  )
  
}