## AWS Config recorder
resource "aws_config_configuration_recorder" "pristine_poc_recoder" {
    name = "pristine_poc_recoder"
    role_arn = var.aws_config_role


    recording_group {
        all_supported = true
        include_global_resource_types = true
    }
 
}

## AWS Config deivery channel
resource "aws_config_delivery_channel" "pristine_poc_delivery" {
    name = "pristine_poc_delivery"
    s3_bucket_name = var.s3_aws_config

    depends_on = [aws_config_configuration_recorder.pristine_poc_recoder]
}

## AWS Config recording status
resource "aws_config_configuration_recorder_status" "config" {
    name = aws_config_configuration_recorder.pristine_poc_recoder.name
    is_enabled = true

    depends_on = [aws_config_delivery_channel.pristine_poc_delivery]
    
}

## AWS Config rule for s3 bucket version check
resource "aws_config_config_rule" "s3_bucket_versioning_enabled" {
    name = "s3_bucket_versioning_enabled"
    source {
        owner = "AWS"
        source_identifier = "S3_BUCKET_VERSIONING_ENABLED"
        }
    depends_on = [aws_config_configuration_recorder.pristine_poc_recoder]
   tags = merge(
    var.additional_tags,
    {
      Name = "${var.name_prefix}_Config_Rule"
    }
  )
}

